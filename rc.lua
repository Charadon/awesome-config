metakey = 'Mod4'
altkey = 'Mod1'
-- Workspaces
awful.tag(
	{ "", "", "", "", "", "", "", "", "1", "2" },
	s,
	awful.layout.layouts[1]
)

-- Keyboard Shortcuts
awful.key(
	{metakey, altkey}, 'q', 
	awesome.quit
),
awful.key(
	{metakey, altkey}, 'r',
	awesome.restart
),
awful.key(
	{metakey}, 'r',
	awful.spawn("rofi")
),
awful.key(
	{metakey, control}, 't',
	awful.spawn("kitty")
),
awful.key(
	{metakey, control}, 'f',
	awful.spawn("nemo")
)